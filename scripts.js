const bankButton = document.getElementById("bankButton");
const loanButton = document.getElementById("loanButton");
const bankBalance = document.getElementById("bankBalance");
const pay = document.getElementById("pay");
const workButton = document.getElementById("workButton");
const getLoanButton = document.getElementById("getLoanButton")
const loanInput = document.getElementById("loanInput")
const invalidText = document.getElementById("invalidAmount");
const repayLoanButton = document.getElementById("repayLoanButton");
const loanText = document.getElementById("loan");
const loanMoney = document.getElementById("loanMoney");
const selectBox = document.getElementById("selectLaptops");
const laptopInfo = document.getElementById("laptopInfo");
const laptopPrice = document.getElementById("laptopPrice");
const laptopTitle = document.getElementById("laptopTitle");
const laptopImg = document.getElementById("laptopImg");
const laptopButton = document.getElementById("laptopButton");
const laptopBuyText = document.getElementById("laptopBuyText");
const laptopSpecs = document.getElementById("laptopSpecs");

let payBalance = 0;
let balance = 0;
let loanAmount = 0;
let hasLoan = false;
let laptopCost = 0;

loanText.style.visibility="hidden";
repayLoanButton.style.visibility="hidden";
workButton.addEventListener("click", addPayBalance);
bankButton.addEventListener("click", moneyToBank);
getLoanButton.addEventListener("click", addLoan);
repayLoanButton.addEventListener("click", repayLoan);
selectBox.addEventListener("change", changeLaptop);
laptopButton.addEventListener("click", buyLaptop);


function buyLaptop(){
    if(balance>laptopCost){
        balance-=laptopCost;
        bankBalance.innerHTML = balance + " kr"
        laptopBuyText.innerHTML="Congratulations on your new laptop!"
    }
    else laptopBuyText.innerHTML="You cannot afford this laptop :("

}

function changeLaptop(){
    let value = selectBox.value;
    getAllPosts()
    .then(function(posts){
        for(i of posts) {
            if(i.title==value){
                laptopTitle.innerHTML=i.title;
                laptopInfo.innerHTML=i.description;
                laptopCost = i.price;
                laptopPrice.innerHTML = laptopCost + " kr";
                laptopSpecs.innerHTML=i.specs;
                laptopImg.src="https://noroff-komputer-store-api.herokuapp.com/"+i.image;
            }
        }
    })
    laptopBuyText.innerHTML="";
}

function getAllPosts() {
    // Retrieve all the Posts from the JSON Placeholder API.
    return fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    // Parse the response received toa JSON object
    .then(function(response) {
        return response.json()
    })
}

// You can still use.then() as a Promise is returned
getAllPosts()
// The posts are available here
.then(function(posts) {
    let laptops=posts;
    for(let i=0;i<laptops.length;i++){
        let opt = document.createElement('option');
        opt.value=laptops[i].title;
        opt.innerHTML=laptops[i].title;
        selectBox.appendChild(opt);
    }
    laptopTitle.innerHTML=laptops[0].title;
    laptopInfo.innerHTML=laptops[0].description;
    laptopCost = laptops[0].price;
    laptopSpecs.innerHTML=laptops[0].specs;
    laptopPrice.innerHTML = laptopCost + " kr";
    laptopImg.src="https://noroff-komputer-store-api.herokuapp.com/"+laptops[0].image;

})


function addPayBalance(){
    payBalance+=100;
    pay.innerHTML = payBalance + " kr";
}

function repayLoan(){
    if(loanAmount>payBalance){
        loanAmount-=payBalance;
        payBalance=0;
        loanMoney.innerHTML = loanAmount + " kr";
    }
    else{
        payBalance-=loanAmount;
        loanAmount=0;
        loanText.style.visibility="hidden";
        repayLoanButton.style.visibility="hidden";
        loanMoney.innerHTML = "";
        hasLoan=false;
    }
    pay.innerHTML = payBalance + " kr";
}

function moneyToBank(){
    if(hasLoan){
        tenPercent = parseInt(payBalance*0.1);
        if(loanAmount<=tenPercent) {
            loanAmount = 0;
            loanText.style.visibility="hidden";
            repayLoanButton.style.visibility="hidden";
            loanMoney.innerHTML = "";
            hasLoan=false;
        }    
        else {
            loanAmount-=tenPercent;
            loanMoney.innerHTML = loanAmount + " kr";
        }
        payBalance-=tenPercent;
    }
    balance+=payBalance
    payBalance=0;
    bankBalance.innerHTML = balance + " kr";
    pay.innerHTML = payBalance + " kr";
}

function addLoan(){
    loanNumber = parseInt(loanInput.value)
    if(loanNumber > balance*2){
        invalidText.innerHTML = "You cannot loan so much";
        return
    }
    if(hasLoan){
        invalidText.innerHTML = "You cannot have more than one loan at a time";
        return
    }
    loanAmount+=loanNumber;
    balance+=loanNumber;
    bankBalance.innerHTML = balance + " kr";
    loanMoney.innerHTML = loanAmount + " kr";    
    hasLoan=true;
    repayLoanButton.style.visibility="visible";
    loanText.style.visibility="visible";
}

